const fs = require('fs');
const util = require('util');
const _ = require('lodash');
const centerOfMass = require('@turf/center-of-mass').default;
const truncate = require('@turf/truncate').default;
const multilinestring = require('turf-multilinestring');

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.GREAT_LAKES);
	const shapes = await readJson("../../input/geojson/usa/great-lakes.geojson");

	const notes = [];

	for (const feature of shapes.features) {
		const name = feature.properties.name;
		const audioId = await downloader.download(name);

		const filename = await writeGeoJSON(feature, name);
		const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;

		notes.push({
			name,
			audioId: `${downloader.configuration.prefix}_${audioId}`,
			featureType: 'lake',
			latitude,
			longitude,
			zoom: 4,
			primaryFeatures: `['/${filename}']`,
			tags: 'geography great-lakes'
		});
	}
	await writeCSV(notes);
}

async function writeGeoJSON(feature, name) {
	const filename = `_geojson-great-lakes_${name.toLowerCase().split(' ').join('-')}.js`;
	const path = `output/geojson/great-lakes/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/great-lakes.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, audioId, featureType, latitude, longitude, zoom, primaryFeatures, tags} = note;
		const line = `${name}\t${audioId}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${primaryFeatures}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
