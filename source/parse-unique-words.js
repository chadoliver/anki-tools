const csv = require('csvtojson');
const _ = require('lodash');
const fs = require('fs');

const dictionary = {};

csv({delimiter: '\t'})
	.fromFile('./1k-sentences.txt')
	.then((rows)=>{
		console.log('num sentences:', rows.length);

		for (const row of rows) {
			const sentence = row.germanSentence;
			for (const rawWord of sentence.split(' ')) {
				const canonicalWord = rawWord.toLowerCase().replace(/[?!,.]/g,'');
				if (dictionary[canonicalWord] === undefined) {
					dictionary[canonicalWord] = {
						ipa: '',
						usages: [sentence]
					};
				} else {
					dictionary[canonicalWord].usages.push(sentence);
				}
			}
		}

		console.log('num unique words:', _.size(dictionary));
		// console.log('unique words:', dictionary);

		// fs.appendFile('dictionary-1.json', JSON.stringify(dictionary), (err) => {
		// 	if (err) {
		// 		console.error(err);
		// 	} else {
		// 		console.log('wrote file to disk');
		// 	}
		// });
	});
