const fs = require('fs');
const util = require('util');
const centerOfMass = require('@turf/center-of-mass').default;
const truncate = require('@turf/truncate').default;

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.CHINA_SUBDIVISION);

	const shapes = await readJson("../input/china/shapes.geojson");
	const subdivisions = await readJson("../input/china/subdivisions.json");

	const notes = [];
	for (const subdivision of subdivisions) {
		const audioId = await downloader.download(subdivision.ckjName);
		const feature = shapes.features.find((f) => f.properties.NAME === subdivision.ckjName);
		const filename = await writeGeoJSON(feature, subdivision.latinName);
		const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;

		notes.push({
			name: subdivision.latinName,
			chineseCharacters: subdivision.ckjName,
			type: subdivision.subdivisionType,
			pinyin: subdivision.pinyin,
			audioId: `${downloader.configuration.prefix}_${audioId}`,
			geojson: `['/${filename}']`,
			latitude,
			longitude,
			zoom: 4
		});
	}
	await writeCSV(notes);
}

async function writeGeoJSON(feature, latinName) {
	const filename = `_geojson_china-subdivision_${latinName.toLowerCase().split(' ').join('-')}.js`;
	const path = `output/geojson/china-subdivisions/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/china-subdivisions.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, chineseCharacters, type, pinyin, audioId, geojson, latitude, longitude, zoom} = note;
		const line = `${name}\t${chineseCharacters}\t${type}\t${pinyin}\t${audioId}\t${geojson}\t${latitude}\t${longitude}\t${zoom}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
