const fs = require('fs');
const util = require('util');
const _ = require('lodash');
const centerOfMass = require('@turf/center-of-mass').default;
const truncate = require('@turf/truncate').default;
const multilinestring = require('turf-multilinestring');

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.GB_COUNTIES);
	const shapes = await readJson("../../input/geojson/gb/GBR_adm2.json");

	const notes = [];

	for (const feature of shapes.features) {
		const name = feature.properties.NAME_2;
		const audioId = await downloader.download(name);
		const audioFilename = `_${downloader.configuration.prefix}_${audioId}_full-speed.mp3`;

		const geojsonFilename = await writeGeoJSON(feature, name);
		const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;

		notes.push({
			name,
			audioFilename,
			featureType: feature.properties.TYPE_2,
			latitude,
			longitude,
			zoom: 7,
			primaryFeatures: `['/${geojsonFilename}']`,
			tags: `geography counties ${feature.properties.NAME_1.split(' ').join('-')}`
		});
	}
	await writeCSV(notes);
}

async function writeGeoJSON(feature, name) {
	const filename = `_geojson-gb-county_${name.toLowerCase().split(' ').join('-')}.js`;
	const path = `output/geojson/gb-counties/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/gb-counties.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, audioFilename, featureType, latitude, longitude, zoom, primaryFeatures, tags} = note;
		const line = `${name}\t${audioFilename}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${primaryFeatures}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
