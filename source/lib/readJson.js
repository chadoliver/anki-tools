const fs = require('fs');

module.exports = function readJson(path) {
	return new Promise((resolve, reject) => {
		fs.readFile(require.resolve(path), (err, data) => {
			if (err)
				reject(err);
			else
				resolve(JSON.parse(data));
		})
	})
};
