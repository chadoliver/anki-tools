process.env.GOOGLE_APPLICATION_CREDENTIALS = "../../token.json";

const textToSpeech = require("@google-cloud/text-to-speech");
const fs = require("fs");
const util = require("util");
const uuidv4 = require("uuid/v4");

const speeds = require("./speeds.json");
const voices = require("./voices.json");

const presets = {
  COUNTRIES: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "audio-english-countries",
    folder: "countries",
  },
  NZ_STATE_HIGHWAYS: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "audio-nz-state-highway",
    folder: "state-highways",
  },
  GREAT_LAKES: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "audio-great-lake",
    folder: "great-lakes",
  },
  MAJOR_SYSTEM: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "major-system",
    folder: "major-system",
  },
  ENGLISH_PLACENAME: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "audio-english-placename",
    folder: "english-placenames",
  },
  SIMPLE_ENGLISH_PLACENAME: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "audio-english-placename",
    folder: "english-placenames",
  },
  GB_COUNTIES: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST],
    prefix: "audio-gb-county",
    folder: "gb-counties",
  },
  GERMAN_PLACENAME: {
    voice: voices.GERMAN,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "audio-german-placename",
    folder: "german-placenames",
  },
  GERMAN_WORD: {
    voice: voices.GERMAN,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "german-word",
    folder: "german-words",
  },
  GERMAN_PHRASE: {
    voice: voices.GERMAN,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "german-phrase",
    folder: "german-phrases",
  },
  CHINA_SUBDIVISION: {
    voice: voices.MANDARIN,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "audio-china-subdivision",
    folder: "china-subdivisions",
  },
  GERMAN_STATE: {
    voice: voices.ENGLISH,
    speeds: [speeds.FAST, speeds.SLOW],
    prefix: "audio-german-state",
    folder: "german-states",
  },
};

class AudioDownloader {
  static client = new textToSpeech.TextToSpeechClient();

  configuration = null;
  writeFile = util.promisify(fs.writeFile);

  constructor(configuration) {
    this.configuration = configuration;
  }

  async download(text) {
    const id = uuidv4();
    await Promise.all(
      this.configuration.speeds.map((speed) =>
        this.downloadVersion(text, id, speed)
      )
    );
    // console.log(`${text}\t\t${id}`);
    console.log(`${text}\t\t${this.configuration.prefix}_${id}`);
    return id;
  }

  async downloadVersion(text, id, speed) {
    const response = await AudioDownloader.client.synthesizeSpeech({
      input: { text },
      voice: this.configuration.voice,
      audioConfig: {
        audioEncoding: "MP3",
        speakingRate: speed.rate,
        volumeGainDb: 0,
      },
    });
    const audio = response[0].audioContent;
    const { folder, prefix } = this.configuration;
    const path = AudioDownloader.getPath(folder, prefix, id, speed);
    await this.writeFile(path, audio, "binary");
  }

  static getPath(folder, prefix, id, speed) {
    return `output/audio/${folder}/_${prefix}_${id}_${speed.fragment}.mp3`;
  }
}

module.exports = {
  AudioDownloader,
  presets,
};
