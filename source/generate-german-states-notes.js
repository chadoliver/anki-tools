const fs = require('fs');
const util = require('util');
const centerOfMass = require('@turf/center-of-mass').default;
const truncate = require('@turf/truncate').default;

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.GERMAN_STATE);

	const shapes = await readJson("../../input/geojson/germany/german-states.geojson");

	const notes = [];
	for (const feature of shapes.features) {
		const name = feature.properties.NAME;
		const audioId = await downloader.download(name);
		const filename = await writeGeoJSON(feature, name);
		const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;

		notes.push({
			name,
			ipa: '',
			audioId: `${downloader.configuration.prefix}_${audioId}`,
			featureType: 'German state',
			latitude,
			longitude,
			zoom: 6,
			primaryFeatures: `['/${filename}']`,
			pronunciationLanguage: 'English',
			tags: 'states-of-germany'
		});
	}
	await writeCSV(notes);
}

async function writeGeoJSON(feature, name) {
	const filename = `_geojson-german-state_${name.toLowerCase().split(' ').join('-')}.js`;
	const path = `output/geojson/german-states/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/german-states.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, ipa, audioId, featureType, latitude, longitude, zoom, primaryFeatures, pronunciationLanguage, tags} = note;
		const line = `${name}\t${ipa}\t${audioId}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${primaryFeatures}\t${pronunciationLanguage}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
