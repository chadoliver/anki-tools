const textToSpeech = require('@google-cloud/text-to-speech');
const csv = require('csvtojson');
const _ = require('lodash');
const fs = require('fs');
const util = require('util');

const readFile = util.promisify(fs.readFile);

async function main() {
	const notes = await getNoteObjects();
	const dictionary = await getIPADictionary();

	for (const note of notes) {
		note.ipa = _.words(note.germanSentence)
			.map((word) => word.toLowerCase())
			.map((word) => {
				const key = word.toLowerCase();
				if (dictionary[key]) {
					const ipa = dictionary[key].ipa;
					if (ipa) {
						return `<span class="ipa-word">${ipa}</span>`;
					} else {
						return `<span class="ipa-missing-transcription"></span>`;
					}
				} else {
					return `<span class="ipa-no-entry"></span>`;
				}
			})
			.join(' ');
	}
	await writeCSV(notes);
}

async function getNoteObjects () {
	return JSON.parse(await readFile('1k-sentence-objects.json'));
}

async function getIPADictionary () {
	return JSON.parse(await readFile('dictionary.json'));
}

async function writeCSV(objects) {
	const path = 'notes.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	for ({difficultyIndex, newWord, germanSentence, englishSentence, germanAudioId, ipa} of objects) {
		stream.write(`${difficultyIndex}\t${newWord}\t${germanSentence}\t${ipa}\t${englishSentence}\t${germanAudioId}\n`);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
