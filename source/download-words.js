const {AudioDownloader, presets} = require("./lib/AudioDownloader");

// process.env.GOOGLE_APPLICATION_CREDENTIALS = "C:/Users/chad.oliver/Development/personal/anki-tools/My First Project-f50c702a4e9c.json";

async function main() {
	const downloader = new AudioDownloader(presets.COUNTRIES);
	const words = [
		"Martinique"
	];

	for (const word of words) {
		await downloader.download(word);
	}
}

main().catch(console.error);
