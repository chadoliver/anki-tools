const textToSpeech = require('@google-cloud/text-to-speech');
const csv = require('csvtojson');
const _ = require('lodash');
const fs = require('fs');
const util = require('util');
const uuidv4 = require('uuid/v4');

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const client = new textToSpeech.TextToSpeechClient();
	const objects = await getNoteObjects();
	for (const object of objects) {
		object.germanAudioId = await createAndSaveAudioFiles(client, object.germanSentence);
	}
	await writeJSON(objects);
}

async function getNoteObjects () {
	const rows = await csv({delimiter: '\t'}).fromFile('./1k-sentences.txt');
	return rows.map(({difficultyIndex, newWord, germanSentence, englishSentence}) => {
		return {difficultyIndex, newWord, germanSentence, englishSentence};
	});
}

async function createAndSaveAudioFiles(client, text) {
	const id = uuidv4();
	await createAndSaveAudioFile(client, `german-${id}-full-speed`, 1, text);
	await createAndSaveAudioFile(client, `german-${id}-half-speed`, 0.5, text);
	return id;
}

async function createAndSaveAudioFile(client, filename, rate, text) {
	const audio = await downloadAudio(client, rate, text);
	await writeAudio(audio, filename);
}

async function downloadAudio(client, rate, text) {
	const [response] = await client.synthesizeSpeech({
		input: {text},
		voice: {
			name: "de-DE-Wavenet-B",
			languageCode: 'de-DE',
			ssmlGender: 'MALE',
		},
		audioConfig: {
			audioEncoding: 'MP3',
			speakingRate: rate,
			volumeGainDb: 0
		},
	});
	return response.audioContent;
}

async function writeAudio(audio, filename) {
	const path = `german-sentence-audio/${filename}.mp3`;
	await writeFile(path, audio, 'binary');
	console.log(`Audio content written to file: ${path}`);
}

async function writeJSON(objects) {
	const path = '1k-sentence-objects.json';
	await writeFile(path, JSON.stringify(objects));
	console.log(`JSON objects written to file: ${path}`);
}

main().catch(console.error);
