const fs = require('fs');
const util = require('util');
const _ = require('lodash');
const bbox = require('@turf/bbox').default;
const truncate = require('@turf/truncate').default;
const multilinestring = require('turf-multilinestring');

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.NZ_STATE_HIGHWAYS);
	const shapes = await readJson("../../input/geojson/new-zealand/nz-state-highway-centrelines.geojson");

	const notes = [];

	const featureGroups = _.chain(shapes.features)
		.groupBy((feature) => feature.properties.SH.replace(/^0+/, ''))
		.map((features, highwayNumber) => ({highwayNumber, features}))
		.sortBy(({highwayNumber}) => highwayNumber)
		.sortBy(({highwayNumber}) => parseInt(_.first(highwayNumber.match(/^\d+/))))
		.value();

	for (const {highwayNumber, features} of featureGroups) {
		const audioId = await downloader.download(`State Highway ${highwayNumber}`);
		const geojson = multilinestring(features.map(feature => {
			return feature.geometry.coordinates.map(([x, y, z]) => [x, y]);
		}));

		const filename = await writeGeoJSON(geojson, highwayNumber);
		const boundingBox = bbox(geojson);
		const longitude = (boundingBox[0] + boundingBox[2]) / 2;
		const latitude = (boundingBox[1] + boundingBox[3]) / 2;

		notes.push({
			highwayNumber,
			audioId: `${downloader.configuration.prefix}_${audioId}`,
			featureType: 'state highway',
			latitude,
			longitude,
			zoom: 4,
			primaryFeatures: `['/${filename}']`,
			tags: 'nz-state-highways'
		});
	}
	await writeCSV(notes);
}

async function writeGeoJSON(feature, name) {
	const filename = `_geojson-nz-state-highway_${name}.js`;
	const path = `output/geojson/state-highways/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/state-highways.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {highwayNumber, audioId, featureType, latitude, longitude, zoom, primaryFeatures, tags} = note;
		const line = `${highwayNumber}\t${audioId}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${primaryFeatures}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
