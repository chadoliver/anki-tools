const fs = require('fs');
const util = require('util');
const _ = require('lodash');
const centerOfMass = require('@turf/center-of-mass').default;
const truncate = require('@turf/truncate').default;

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const readJson = require("./lib/readJson");

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const downloader = new AudioDownloader(presets.COUNTRIES);
	const shapes = await readJson("../../input/geojson/world/countries/countries.geojson");

	const text = _.chain(shapes.features)
		.map(feature => {
			return {
				name: feature.properties.NAME_LONG,
				type: feature.properties.TYPE
			};
		})
		.sortBy(({name}) => name)
		.sortBy(({type}) => type)
		.map(({name, type}) => `${name}		${type}`)
		.value();

	console.log(text.join('\n'));

	// const notes = [];
	// for (const feature of shapes.features) {
	// 	const name = feature.properties.NAME_LONG;
	// 	const audioId = await downloader.download(name);
	// 	const filename = await writeGeoJSON(feature, name);
	// 	const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;
	//
	// 	notes.push({
	// 		name,
	// 		ipa: '',
	// 		audioId: `${downloader.configuration.prefix}_${audioId}`,
	// 		featureType: 'country',
	// 		latitude,
	// 		longitude,
	// 		zoom: 4,
	// 		primaryFeatures: `['/${filename}']`,
	// 		pronunciationLanguage: 'English',
	// 		tags: 'countries'
	// 	});
	// }
	// await writeCSV(notes);
}

async function writeGeoJSON(feature, name) {
	const filename = `_geojson-country_${name.toLowerCase().split(' ').join('-')}.js`;
	const path = `output/geojson/countries/${filename}`;
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
	return filename;
}

async function writeCSV(notes) {
	const path = './output/notes/countries.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, ipa, audioId, featureType, latitude, longitude, zoom, primaryFeatures, pronunciationLanguage, tags} = note;
		const line = `${name}\t${ipa}\t${audioId}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${primaryFeatures}\t${pronunciationLanguage}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);
