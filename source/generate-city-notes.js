const csv = require('csvtojson');
const fs = require('fs');
const util = require('util');
const _ = require('lodash');
const bbox = require('@turf/bbox').default;
const truncate = require('@turf/truncate').default;
const multilinestring = require('turf-multilinestring');

const {AudioDownloader, presets} = require("./lib/AudioDownloader");
const writeFile = util.promisify(fs.writeFile);

const downloader = new AudioDownloader(presets.SIMPLE_ENGLISH_PLACENAME);

const countries = {
	'GB': 'Great Britain',
	'IE': 'Ireland'
};

const minPlaceSize = {
	city: 50000,
	town: 500,
	village: 0
};

async function main() {
	const allRows = await csv({delimiter: '\t'}).fromFile('./input/csv/cities500.csv');

	const places = allRows
		.filter(row => {
			const isRightCountry = Object.keys(countries).includes(row.countrycode);
			const isBigEnough = row.population >= minPlaceSize.city;
			const isImportantEnough = ['PPLC', 'PPLCH', 'PPLA', 'PPLA2'].includes(row.featurecode);
			return isRightCountry && (isBigEnough || isImportantEnough);
		})
		.sort((a, b) => Number(b.population) - Number(a.population));

	// console.log(places);

	const notes = await Promise.all(places.map(getNote));
	await writeCSV(notes);
}

async function getNote(place) {
	const {name, latitude, longitude} = place;
	const audioId = await downloader.download(place.name);
	const filename = `_${downloader.configuration.prefix}_${audioId}_full-speed.mp3`;
	const featureType = place.population >= minPlaceSize.city ? 'city' : 'town';
	const zoom = 7;
	const country = countries[place.countrycode];
	const tags = `geography cities ${country.split(' ').join('-')}`;

	return {name, filename, featureType, latitude, longitude, zoom, tags};
}

async function writeCSV(notes) {
	const path = './output/notes/gb-cities.csv';

	const stream = fs.createWriteStream(path, {flags:'a'});
	await new Promise((resolve) => stream.on('open', () => resolve()));

	for (const note of notes) {
		const {name, filename, featureType, latitude, longitude, zoom, tags} = note;
		const line = `${name}\t${filename}\t${featureType}\t${latitude}\t${longitude}\t${zoom}\t${tags}\n`;
		stream.write(line);
	}
	stream.end();

	console.log(`csv data written to file: ${path}`);
}

main().catch(console.error);

