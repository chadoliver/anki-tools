process.env.GOOGLE_APPLICATION_CREDENTIALS = "./token.json";

const textToSpeech = require("@google-cloud/text-to-speech");
const fs = require("fs");
const util = require("util");

const writeFile = util.promisify(fs.writeFile);

async function main() {
  const client = new textToSpeech.TextToSpeechClient();

  // const letters = "abcdefghijklmnopqrstuvwxyz".split("");
  const letters = "eigjqrstz".split("");
  console.log(letters);

  for (const letter of letters) {
    await createAudio(client, [{ filename: letter, text: letter }]);
  }
}

async function createAudio(client, tasks) {
  for (const { text, filename } of tasks) {
    const audio = await makeRequest(client, text);
    await writeAudio(audio, filename);
  }
}

async function makeRequest(client, text) {
  const [response] = await client.synthesizeSpeech({
    input: { text },
    voice: {
      name: "fr-FR-Wavenet-D",
      languageCode: "fr-FR",
      ssmlGender: "MALE",
    },
    audioConfig: {
      audioEncoding: "MP3",
      speakingRate: 0.6,
      volumeGainDb: 0,
    },
  });
  return response.audioContent;
}

async function writeAudio(audio, filename) {
  const path = `output/french-audio/alphabet/${filename}.mp3`;
  await writeFile(path, audio, "binary");
  console.log(`Audio content written to file: ${path}`);
}

main().catch(console.error);
