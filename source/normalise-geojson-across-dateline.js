const _ = require('lodash');
const fs = require('fs');
const util = require('util');
const truncate = require('@turf/truncate').default;
const centerOfMass = require('@turf/center-of-mass').default;

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const name = 'wallis-and-futuna-islands';
	const inputPath = `../input/geojson/across-dateline/_geojson-country_${name}.js`;
	const outputPath = `./output/geojson/across-dateline/_geojson-country_${name}_normalised.js`;

	const feature = require(inputPath);
	normaliseCoordinates(feature.geometry.coordinates);
	const [longitude, latitude] = centerOfMass(feature).geometry.coordinates;
	await writeGeoJSON(feature, outputPath);
	console.log(feature.properties.NAME);
	console.log(`path: ['/_geojson-country_${name}_normalised.js']`);
	console.log(`lat ${latitude}`);
	console.log(`lon ${longitude}`);
}

function normaliseCoordinates(array) {
	if (_.every(array, (element) => _.isNumber(element))) {
		if (array.length !== 2) {
			throw new Error('length != 2');
		}
		if (array[0] < 0) {
			array[0] = array[0] + 360;
		}
	} else {
		for (const element of array) {
			if (_.isArray(element)) {
				normaliseCoordinates(element);
			}
		}
	}
}

async function writeGeoJSON(feature, path) {
	const data = `export const feature = ${JSON.stringify(truncate(feature))};`;
	await writeFile(path, data);
}

main().catch(console.error);
