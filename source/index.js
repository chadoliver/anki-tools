const textToSpeech = require('@google-cloud/text-to-speech');
const fs = require('fs');
const util = require('util');

const writeFile = util.promisify(fs.writeFile);

async function main() {
	const client = new textToSpeech.TextToSpeechClient();

	await createAudio(client, [
		{filename: 'output', text: 'Fahrtenbuch'},
	]);
}

async function createAudio(client, tasks) {
	for (const {text, filename} of tasks) {
		const audio = await makeRequest(client, text);
		await writeAudio(audio, filename);
	}
}

async function makeRequest(client, text) {
	const [response] = await client.synthesizeSpeech({
		input: {text},
		voice: {
			name: "de-DE-Wavenet-B",
			languageCode: 'de-DE',
			ssmlGender: 'MALE',
		},
		audioConfig: {
			audioEncoding: 'MP3',
			speakingRate: 0.9,
			volumeGainDb: 0
		},
	});
	return response.audioContent;
}

async function writeAudio(audio, filename) {
	const path = `output/${filename}.mp3`;
	await writeFile(path, audio, 'binary');
	console.log(`Audio content written to file: ${path}`);
}

main().catch(console.error);
